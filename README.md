# SMS2NEST #
## A Twilio-enabled SMS Nest Thermostat Controller ##
Control your Nest thermostat over SMS. Don't have a smartphone or want to give friends house sitting the ability to set the thermostat remotely? SMS2NEST is the answer.

## Installation / Use ##
1. Grab a Twilio account.
2. Grab a Twilio phone number.
3. Configure that number's SMS Request URL to point to your sms-hander.php
4. Rename config.example.php to config.php and edit it to reflect your nest credentials. 
5. Send a text to your number. See below for the syntax

## SMS Syntax ##

### Setting the temperature ###
*set temp <temperature>*

#### Example ####
*set temp 88* would set the temperature to 88F (or 88C if you are in Celcius). Automatically sets it to "home" if "away"

### Setting the house state ###
*set state <home|away>*

#### Example ####
*set state home* would set the state to "home"

### Getting the temperature ###
*get temp*

#### Example ####
*get temp* would return the current temp and the set (target) temp

### Getting the HVAC state ###
*get hvac*

#### Example ####
*get hvac* would return the current AC state and the FAN state

### Getting the House state ###
*get stae*

#### Example ####
*get state* would return the current "home" or "away" state 

## To Do / Issues ##
1. Config.php should probably be protected from prying eyes. This can easily be fixed by checking for a pre-defined PHP variable which would only be defined in sms-handler.php
2. Anyone can control your thermostat if they know your Twilio number. This is easy to fix my simply implementing a check against a list of acceptable numbers
3. Adding on to the above, it should be easy to add and remove numbers if you want to give and remove access to friends/family