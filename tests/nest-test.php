<?php
// include the nest api library
require('../services/Nest/nest-php-api.php');

// create new nest object w/ login information
$nest = new Nest('email@address.tld', 'password');

// print everything
//var_dump($nest->status_get());

echo "Current Temperature: " . $nest->temperature_current_get() . "<BR>/";
echo "Target Temperature: " . $nest->temperature_target_get() . "<BR>/";
echo "HVAC AC State: " . $nest->hvac_ac_state_get() . "<BR>/";
echo "HVAC FAN State: " . $nest->hvac_fan_state_get() . "<BR>/";

?>