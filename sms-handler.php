<?php
// "Psuedo" Natural Language Process
// include the nest api library
require('services/Nest/nest-php-api.php');
require('config.php');

// create new nest object w/ login information
$nest = new Nest(NEST_USERNAME, NEST_PASSWORD);

$pieces = explode(" ",strtolower($_REQUEST['Body']));
$response = "";

try {
	switch($pieces[0])
	{
		case "set":
			switch($pieces[1])
			{
				case "temp":
					$home_state = $nest->house_state_get();
					if ($home_state == "away")
					{
						$nest->house_state_set("home");
					}
					if (is_numeric($pieces[2]))
					{
						$response = "Setting temp to " . $pieces[2];
						$nest->temperature_set($pieces[2]);
					}
					else
					{
						throw new Exception('Set temp is not a number!');
					}
					break;

				case "state":
					if ($pieces[2] == "home")
					{
						$nest->house_state_set("home");
						$response = "Setting state to " . $pieces[2];
					}
					elseif ($pieces[2] == "away")
					{
						$nest->house_state_set("away");
						$response = "Setting state to " . $pieces[2];
					}
					else
					{
						throw new Exception('Unknown house state');
					}
					break;

				default:
					throw new Exception('Unknown set command');
			}
		break;

		case "get":
			switch($pieces[1])
			{
				case "temp":
					$response = "Curr Temp: " . $nest->temperature_current_get() . " Set Temp: " . $nest->temperature_target_get();
					break;

				case "hvac":
					$ac_state = $nest->hvac_ac_state_get();
					$fan_state = $nest->hvac_fan_state_get();
					

					if($ac_state)
					{
						$response .= "AC ON ";
					}
					else
					{
						$response .= "AC OFF ";
					}
					if($fan_state)
					{
						$response .= "FAN ON ";
					}
					else
					{
						$response .= "FAN OFF ";
					}
					break;

				case "state":
					$home_state = $nest->house_state_get();
					$response .= "State: " . $home_state;
					break;

				default:
					throw new Exception('Unknown get command');
			}
		break;
		
		default:
			throw new Exception('Unknown command: ' . strtolower($_REQUEST['Body']));
	}
} catch (Exception $e) {
	$response = "Error: " . $e->getMessage();
}
?>
<Response>
    <Sms><?php echo $response; ?></Sms>
</Response>